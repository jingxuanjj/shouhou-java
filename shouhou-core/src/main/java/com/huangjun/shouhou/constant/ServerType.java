package com.huangjun.shouhou.constant;

import com.huangjun.common.validator.Check;

import java.util.HashMap;
import java.util.Map;

/**
 * 枚举【服务类型】
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public enum ServerType {

    /**
     * 保内免费
     */
    BAONEI(1, "保内免费"),
    /**
     * 保外免费
     */
    BAOWAI(2, "保外免费"),
    ;


    /**
     * 枚举值罗列，给swagger接口文档展示用
     */
    public static final String VALUES_STR = "1,2";

    private static final Map<Integer, ServerType> LOOKUP = new HashMap<>();

    static {
        for (ServerType e : ServerType.values()) {
            LOOKUP.put(e.value, e);
        }
    }

    private final Integer value;
    private final String desc;


    ServerType(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static ServerType find(Integer value) {
        return LOOKUP.get(value);
    }

    public static ServerType findByDesc(String desc) {
        for (ServerType e : ServerType.values()) {
            if (e.getDesc().equals(desc)) {
                return e;
            }
        }
        return null;
    }


    /**
     * desc映射value
     *
     * @param desc
     * @return
     */
    public static Integer descToValue(String desc) {
        ServerType theEnum = findByDesc(desc);
        if (theEnum != null) {
            return theEnum.getValue();
        }
        return null;
    }

    /**
     * value映射desc
     *
     * @param value
     * @return
     */
    public static String valueToDesc(Integer value) {
        ServerType theEnum = find(value);
        if (theEnum != null) {
            return theEnum.getDesc();
        }
        return null;
    }

    /**
     * 校验有效性
     */
    @Check
    public static final boolean validate(Integer value) {
        ServerType theEnum = find(value);
        return theEnum != null;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }


}

