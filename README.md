# 售后系统管理

## 关于项目

- web项目启动类：[ShouhouApp.java](/shouhou-web/src/main/java/com/huangjun/shouhou/ShouhouApp.java)
- 单元测试入口类：[Main.java](/shouhou-web/src/test/java/com/huangjun/shouhou/Main.java)
- mysql建表语句：[shouhou.sql](/shouhou-web/src/test/resources/DB/shouhou.sql)

## 表结构

### 客户管理【customer】
|字段名 | 类型 | 非空 | 键 | 注释 |
|------ | ---- | --- | --- | ---- |
| customer_id | bigint(20) | 是 | 主键 | 主键ID |
| name | varchar(50) |  否  |  | 客户名称 |
| number | varchar(50) |  否  |  | 客户编号 |
| phone_name | varchar(32) |  否  |  | 联系人 |
| phone | varchar(32) |  否  |  | 联系电话 |
| address | varchar(100) |  否  |  | 地址 |
| owner | varchar(32) |  否  |  | 客户负责人 |
| created_time | datetime | 是 |  | 创建时间【yyyy-MM-dd HH:mm:ss】 |
| created_by | varchar(20) | 是 |  | 创建人【最大长度20】 |
| operated_time | datetime | 是 |  | 修改时间【yyyy-MM-dd HH:mm:ss】 |

### 产品管理【product】
|字段名 | 类型 | 非空 | 键 | 注释 |
|------ | ---- | --- | --- | ---- |
| product_id | bigint(20) | 是 | 主键 | 主键ID |
| name | varchar(50) |  否  |  | 产品名称 |
| number | varchar(50) |  否  |  | 产品编号 |
| type | int(11) |  否  |  | 产品分类：具体详细请见 枚举管理查看 |
| created_time | datetime | 是 |  | 创建时间【yyyy-MM-dd HH:mm:ss】 |
| created_by | varchar(20) | 是 |  | 创建人【最大长度20】 |
| operated_time | datetime | 是 |  | 修改时间【yyyy-MM-dd HH:mm:ss】 |

### 用户表(员工表)【user】
|字段名 | 类型 | 非空 | 键 | 注释 |
|------ | ---- | --- | --- | ---- |
| user_id | bigint(20) | 是 | 主键 | 主键ID |
| name | varchar(32) | 是 |  | 姓名 |
| username | varchar(50) | 是 |  | 账号 |
| password | varchar(100) | 是 |  | 密码 |
| phone | varchar(32) |  否  |  | 电话 |
| role | int(11) |  否  |  | 角色 |
| created_time | datetime | 是 |  | 创建时间【yyyy-MM-dd HH:mm:ss】 |
| created_by | varchar(20) | 是 |  | 创建人【最大长度20】 |
| operated_time | datetime | 是 |  | 修改时间【yyyy-MM-dd HH:mm:ss】 |

### 工单中心【order】
|字段名 | 类型 | 非空 | 键 | 注释 |
|------ | ---- | --- | --- | ---- |
| order_id | bigint(20) | 是 | 主键 | 主键ID |
| repair_number | varchar(100) |  否  |  | 关联报修单号 |
| order_number | varchar(50) |  否  |  | 工单编号 |
| customer_contacts | varchar(32) |  否  |  | 客户联系人 |
| phone | varchar(32) |  否  |  | 电话 |
| address | varchar(100) |  否  |  | 地址 |
| server_type | int(11) | 是 |  | 服务类型 |
| money | decimal(20,2) |  否  |  | 本次服务费 |
| remark | varchar(200) |  否  |  | 备注 |
| file_path | varchar(100) |  否  |  | 上传附件 |
| status | int(11) |  否  |  | 工单状态 |
| customer_id | bigint(20) |  否  |  | 客户 |
| product_id | bigint(20) |  否  |  | 客户产品 |
| user_id | bigint(20) |  否  |  | 选择维修工 |
| created_time | datetime | 是 |  | 创建时间【yyyy-MM-dd HH:mm:ss】 |
| operated_time | datetime | 是 |  | 修改时间【yyyy-MM-dd HH:mm:ss】 |

### 报修中心【repair】
|字段名 | 类型 | 非空 | 键 | 注释 |
|------ | ---- | --- | --- | ---- |
| repair_id | bigint(20) | 是 | 主键 | 主键ID |
| repair_number | varchar(50) |  否  |  | 报修单编号 |
| customer_contacts | varchar(32) |  否  |  | 客户联系人 |
| phone | varchar(32) |  否  |  | 电话 |
| address | varchar(100) |  否  |  | 地址 |
| file_path | varchar(100) |  否  |  | 上传附件 |
| repair_type | int(11) | 是 |  | 报修类型 |
| remark | varchar(200) |  否  |  | 备注 |
| status | int(11) |  否  |  | 工单状态 |
| customer_id | bigint(20) |  否  |  | 客户 |
| product_id | bigint(20) |  否  |  | 客户产品 |
| user_id | bigint(20) |  否  |  | 选择维修工 |
| created_time | datetime | 是 |  | 创建时间【yyyy-MM-dd HH:mm:ss】 |
| created_by | varchar(20) |  否  |  | 创建人【最大长度20】 |
| operated_time | datetime | 是 |  | 修改时间【yyyy-MM-dd HH:mm:ss】 |
