package com.huangjun.shouhou.pojo.mapper.user;

import com.huangjun.shouhou.pojo.dto.user.UserAddDTO;
import com.huangjun.shouhou.pojo.dto.user.UserUpdateDTO;
import com.huangjun.shouhou.pojo.po.user.UserPO;
import com.huangjun.shouhou.pojo.vo.user.UserShowVO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

/**
 * 【用户表(员工表)】映射
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    /**
     * addDTO映射po
     *
     * @param userAddDTO
     * @return
     */
    UserPO fromAddDTO(UserAddDTO userAddDTO);

    /**
     * 将updateDTO中的值设置到po
     *
     * @param userPO
     * @param userUpdateDTO
     */
    void setUpdateDTO(@MappingTarget UserPO userPO, UserUpdateDTO userUpdateDTO);

    /**
     * po映射showVO
     *
     * @param userPO
     * @return
     */
    UserShowVO toShowVO(UserPO userPO);


}

