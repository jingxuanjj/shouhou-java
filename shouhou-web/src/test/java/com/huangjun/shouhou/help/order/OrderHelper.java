package com.huangjun.shouhou.help.order;

import com.huangjun.common.util.SafeUtil;
import com.huangjun.shouhou.pojo.dto.order.*;
import com.huangjun.shouhou.pojo.po.order.*;
import com.huangjun.shouhou.service.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import static com.huangjun.shouhou.pojo.example.order.OrderExample.*;

@Component
public class OrderHelper {

    @Autowired
    private OrderService orderService;

    /**
     * 生成add测试数据
     *
     * @return
     */
    public OrderAddDTO getOrderAddDTO(Long customerId, Long productId, Long userId) {
        OrderAddDTO dto = new OrderAddDTO();
        dto.setRepairNumber(E_REPAIR_NUMBER);
        dto.setOrderNumber(E_ORDER_NUMBER);
        dto.setCustomerContacts(E_CUSTOMER_CONTACTS);
        dto.setPhone(E_PHONE);
        dto.setAddress(E_ADDRESS);
        dto.setServerType(SafeUtil.getInteger(E_SERVER_TYPE));
        dto.setMoney(SafeUtil.getBigDecimal(E_MONEY));
        dto.setRemark(E_REMARK);
        dto.setFilePath(E_FILE_PATH);
        dto.setStatus(SafeUtil.getInteger(E_STATUS));
        dto.setCustomerId(customerId);
        dto.setProductId(productId);
        dto.setUserId(userId);
        return dto;
    }


    /**
     * 生成update测试数据
     *
     * @return
     */
    public OrderUpdateDTO getOrderUpdateDTO(OrderPO order) {
        OrderUpdateDTO dto = new OrderUpdateDTO();
        dto.setOrderId(order.getOrderId());
        dto.setRepairNumber(order.getRepairNumber());
        dto.setOrderNumber(order.getOrderNumber());
        dto.setCustomerContacts(order.getCustomerContacts());
        dto.setPhone(order.getPhone());
        dto.setAddress(order.getAddress());
        dto.setServerType(order.getServerType());
        dto.setMoney(order.getMoney());
        dto.setRemark(order.getRemark());
        dto.setFilePath(order.getFilePath());
        dto.setStatus(order.getStatus());
        dto.setCustomerId(order.getCustomerId());
        dto.setProductId(order.getProductId());
        dto.setUserId(order.getUserId());
        return dto;
    }

    /**
     * 保存示例
     *
     * @return
     */
    public OrderPO saveOrderExample(Long customerId, Long productId, Long userId) {
        OrderAddDTO addDTO = this.getOrderAddDTO(customerId, productId, userId);
        return orderService.save(addDTO);
    }


}

