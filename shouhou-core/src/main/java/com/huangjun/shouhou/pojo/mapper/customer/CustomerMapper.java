package com.huangjun.shouhou.pojo.mapper.customer;

import com.huangjun.shouhou.pojo.dto.customer.CustomerAddDTO;
import com.huangjun.shouhou.pojo.dto.customer.CustomerUpdateDTO;
import com.huangjun.shouhou.pojo.po.customer.CustomerPO;
import com.huangjun.shouhou.pojo.vo.customer.CustomerShowVO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

/**
 * 【客户管理】映射
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Mapper
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    /**
     * addDTO映射po
     *
     * @param customerAddDTO
     * @return
     */
    CustomerPO fromAddDTO(CustomerAddDTO customerAddDTO);

    /**
     * 将updateDTO中的值设置到po
     *
     * @param customerPO
     * @param customerUpdateDTO
     */
    void setUpdateDTO(@MappingTarget CustomerPO customerPO, CustomerUpdateDTO customerUpdateDTO);

    /**
     * po映射showVO
     *
     * @param customerPO
     * @return
     */
    CustomerShowVO toShowVO(CustomerPO customerPO);


}

