package com.huangjun.common.pojo.qo;

import com.huangjun.common.util.JsonUtil;

import java.io.Serializable;

/**
 * 数据查询参数对象超类
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public abstract class AbstractQO implements Serializable {

    private static final long serialVersionUID = -2460649808778841614L;

    @Override
    public String toString() {
        return JsonUtil.toJSONString(this);
    }

}

