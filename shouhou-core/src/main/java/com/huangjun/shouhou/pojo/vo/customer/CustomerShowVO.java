package com.huangjun.shouhou.pojo.vo.customer;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huangjun.common.constant.JsonFieldConst;
import com.huangjun.common.pojo.vo.AbstractVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

import static com.huangjun.shouhou.pojo.example.customer.CustomerExample.*;

/**
 * 【客户管理】详情展示对象
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(description = "【客户管理】详情展示对象")
public class CustomerShowVO extends AbstractVO {

    @ApiModelProperty(notes = N_CUSTOMER_ID, example = E_CUSTOMER_ID)
    private Long customerId;

    @ApiModelProperty(notes = N_NAME, example = E_NAME)
    private String name;

    @ApiModelProperty(notes = N_NUMBER, example = E_NUMBER)
    private String number;

    @ApiModelProperty(notes = N_PHONE_NAME, example = E_PHONE_NAME)
    private String phoneName;

    @ApiModelProperty(notes = N_PHONE, example = E_PHONE)
    private String phone;

    @ApiModelProperty(notes = N_ADDRESS, example = E_ADDRESS)
    private String address;

    @ApiModelProperty(notes = N_OWNER, example = E_OWNER)
    private String owner;

    @ApiModelProperty(notes = N_CREATED_TIME, example = E_CREATED_TIME)
    @JsonFormat(pattern=JsonFieldConst.DEFAULT_DATETIME_FORMAT, timezone="GMT+8")
    private LocalDateTime createdTime;

    @ApiModelProperty(notes = N_CREATED_BY, example = E_CREATED_BY)
    private String createdBy;

    @ApiModelProperty(notes = N_OPERATED_TIME, example = E_OPERATED_TIME)
    @JsonFormat(pattern=JsonFieldConst.DEFAULT_DATETIME_FORMAT, timezone="GMT+8")
    private LocalDateTime operatedTime;



}

