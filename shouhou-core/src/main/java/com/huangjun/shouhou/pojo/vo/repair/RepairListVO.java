package com.huangjun.shouhou.pojo.vo.repair;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huangjun.common.constant.JsonFieldConst;
import com.huangjun.common.pojo.vo.AbstractVO;
import com.huangjun.shouhou.constant.RepairStatus;
import com.huangjun.shouhou.constant.RepairType;
import com.huangjun.shouhou.pojo.example.repair.CustomerExample;
import com.huangjun.shouhou.pojo.example.repair.ProductExample;
import com.huangjun.shouhou.pojo.example.repair.UserExample;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

import static com.huangjun.shouhou.pojo.example.repair.RepairExample.*;

/**
 * 【报修中心】列表展示对象
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(description = "【报修中心】列表展示对象")
public class RepairListVO extends AbstractVO {

    @ApiModelProperty(notes = N_REPAIR_ID, example = E_REPAIR_ID)
    private Long repairId;

    @ApiModelProperty(notes = N_REPAIR_NUMBER, example = E_REPAIR_NUMBER)
    private String repairNumber;

    @ApiModelProperty(notes = N_REPAIR_TYPE, example = E_REPAIR_TYPE, allowableValues = RepairType.VALUES_STR)
    private Integer repairType;

    @ApiModelProperty(notes = N_STATUS, example = E_STATUS, allowableValues = RepairStatus.VALUES_STR)
    private Integer status;

    @ApiModelProperty(notes = N_CUSTOMER_ID, example = E_CUSTOMER_ID)
    private Long customerId;

    @ApiModelProperty(notes = N_PRODUCT_ID, example = E_PRODUCT_ID)
    private Long productId;

    @ApiModelProperty(notes = N_CREATED_TIME, example = E_CREATED_TIME)
    @JsonFormat(pattern = JsonFieldConst.DEFAULT_DATETIME_FORMAT, timezone = "GMT+8")
    private LocalDateTime createdTime;

    @ApiModelProperty(notes = N_OPERATED_TIME, example = E_OPERATED_TIME)
    @JsonFormat(pattern = JsonFieldConst.DEFAULT_DATETIME_FORMAT, timezone = "GMT+8")
    private LocalDateTime operatedTime;

    @ApiModelProperty(notes = CustomerExample.N_NAME, example = CustomerExample.E_NAME)
    private String customerName;

    @ApiModelProperty(notes = ProductExample.N_NAME, example = ProductExample.E_NAME)
    private String productName;

    @ApiModelProperty(notes = UserExample.N_NAME, example = UserExample.E_NAME)
    private String userName;



}

