package com.huangjun.shouhou.service.user;

import com.huangjun.common.constant.ErrorCode;
import com.huangjun.common.exception.BusinessException;
import com.huangjun.common.pojo.qo.OptionQO;
import com.huangjun.common.pojo.vo.OptionVO;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.dao.user.OrderDAO;
import com.huangjun.shouhou.dao.user.RepairDAO;
import com.huangjun.shouhou.dao.user.UserDAO;
import com.huangjun.shouhou.pojo.dto.user.UserAddDTO;
import com.huangjun.shouhou.pojo.dto.user.UserUpdateDTO;
import com.huangjun.shouhou.pojo.mapper.user.UserMapper;
import com.huangjun.shouhou.pojo.po.user.UserPO;
import com.huangjun.shouhou.pojo.qo.user.UserQO;
import com.huangjun.shouhou.pojo.vo.user.UserListVO;
import com.huangjun.shouhou.pojo.vo.user.UserShowVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 【用户表(员工表)】增删改查服务
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Service
public class UserService {

    @Autowired
    private OrderDAO orderDAO;
    @Autowired
    private RepairDAO repairDAO;
    @Autowired
    private UserDAO userDAO;


    /**
     * 新增【用户表(员工表)】
     *
     * @param userDTO
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public UserPO save(UserAddDTO userDTO) {
        UserPO user = UserMapper.INSTANCE.fromAddDTO(userDTO);
        userDAO.save(user);
        return user;
    }

    /**
     * 修改【用户表(员工表)】
     *
     * @param userUpdateDTO
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public UserPO update(UserUpdateDTO userUpdateDTO) {
        Long userId = userUpdateDTO.getUserId();
        UserPO user = this.getUser(userId, true);
        UserMapper.INSTANCE.setUpdateDTO(user, userUpdateDTO);
        userDAO.update(user);
        return user;
    }

    /**
     * 查询分页列表
     *
     * @param userQO
     * @return
     */
    public PageVO<UserListVO> list(UserQO userQO) {
        PageVO<UserListVO> page = userDAO.findByPage(userQO);
        return page;
    }

    /**
     * 查询【用户表(员工表)】选项列表
     *
     * @return
     */
    public List<OptionVO<Long, String>> findOptions(OptionQO<Long, String> qo) {
        List<OptionVO<Long, String>> options = userDAO.findOptions(qo);
        return options;
    }

    /**
     * 根据主键获取【用户表(员工表)】
     *
     * @param userId 主键
     * @param force 是否强制获取
     * @return
     */
    public UserPO getUser(Long userId, boolean force) {
        UserPO user = userDAO.findById(userId);
        if (force && user == null) {
            throw new BusinessException(ErrorCode.RECORD_NOT_FIND);
        }
        return user;
    }


    /**
     * 查询【用户表(员工表)】详情
     *
     * @param userId
     * @return
     */
    public UserShowVO show(Long userId) {
        UserPO user = this.getUser(userId, true);
        UserShowVO showVO = UserMapper.INSTANCE.toShowVO(user);
        return showVO;
    }

    /**
     * 删除【用户表(员工表)】
     *
     * @param userIds
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public int delete(Long... userIds) {
        int count = 0;
        for (Long userId : userIds) {
            this.checkDeleteByOrder(userId);
            this.checkDeleteByRepair(userId);
            count += userDAO.delete(userId);
        }
        return count;
    }

    /**
     * 校验是否存在【工单中心】关联
     *
     * @param userId
     */
    private void checkDeleteByOrder(Long userId) {
        int count = orderDAO.getCountByUserId(userId);
        if (count > 0) {
            throw new BusinessException(ErrorCode.CASCADE_DELETE_ERROR);
        }
    }

    /**
     * 校验是否存在【报修中心】关联
     *
     * @param userId
     */
    private void checkDeleteByRepair(Long userId) {
        int count = repairDAO.getCountByUserId(userId);
        if (count > 0) {
            throw new BusinessException(ErrorCode.CASCADE_DELETE_ERROR);
        }
    }


}


