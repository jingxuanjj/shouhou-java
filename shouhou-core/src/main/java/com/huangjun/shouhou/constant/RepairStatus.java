package com.huangjun.shouhou.constant;

import com.huangjun.common.validator.Check;

import java.util.HashMap;
import java.util.Map;

/**
 * 枚举【报修单状态】
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public enum RepairStatus {

    /**
     * 待受理
     */
    DAISHOULI(1, "待受理"),
    /**
     * 待维修
     */
    DAIWEIXIU(2, "待维修"),
    /**
     * 维修中
     */
    WEIXIUZHONG(3, "维修中"),
    /**
     * 待评价
     */
    DAIPINGJIA(4, "待评价"),
    /**
     * 已拒绝
     */
    YIJUEJUE(5, "已拒绝"),
    /**
     * 已关闭
     */
    YIJUJUE(6, "已关闭"),
    ;


    /**
     * 枚举值罗列，给swagger接口文档展示用
     */
    public static final String VALUES_STR = "1,2,3,4,5,6";

    private static final Map<Integer, RepairStatus> LOOKUP = new HashMap<>();

    static {
        for (RepairStatus e : RepairStatus.values()) {
            LOOKUP.put(e.value, e);
        }
    }

    private final Integer value;
    private final String desc;


    RepairStatus(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static RepairStatus find(Integer value) {
        return LOOKUP.get(value);
    }

    public static RepairStatus findByDesc(String desc) {
        for (RepairStatus e : RepairStatus.values()) {
            if (e.getDesc().equals(desc)) {
                return e;
            }
        }
        return null;
    }


    /**
     * desc映射value
     *
     * @param desc
     * @return
     */
    public static Integer descToValue(String desc) {
        RepairStatus theEnum = findByDesc(desc);
        if (theEnum != null) {
            return theEnum.getValue();
        }
        return null;
    }

    /**
     * value映射desc
     *
     * @param value
     * @return
     */
    public static String valueToDesc(Integer value) {
        RepairStatus theEnum = find(value);
        if (theEnum != null) {
            return theEnum.getDesc();
        }
        return null;
    }

    /**
     * 校验有效性
     */
    @Check
    public static final boolean validate(Integer value) {
        RepairStatus theEnum = find(value);
        return theEnum != null;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }


}

