package com.huangjun.shouhou.service.customer;

import com.huangjun.common.constant.ErrorCode;
import com.huangjun.common.exception.BusinessException;
import com.huangjun.common.pojo.qo.OptionQO;
import com.huangjun.common.pojo.vo.OptionVO;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.dao.customer.CustomerDAO;
import com.huangjun.shouhou.dao.customer.OrderDAO;
import com.huangjun.shouhou.dao.customer.RepairDAO;
import com.huangjun.shouhou.pojo.dto.customer.CustomerAddDTO;
import com.huangjun.shouhou.pojo.dto.customer.CustomerUpdateDTO;
import com.huangjun.shouhou.pojo.mapper.customer.CustomerMapper;
import com.huangjun.shouhou.pojo.po.customer.CustomerPO;
import com.huangjun.shouhou.pojo.qo.customer.CustomerQO;
import com.huangjun.shouhou.pojo.vo.customer.CustomerListVO;
import com.huangjun.shouhou.pojo.vo.customer.CustomerShowVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 【客户管理】增删改查服务
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Service
public class CustomerService {

    @Autowired
    private CustomerDAO customerDAO;
    @Autowired
    private OrderDAO orderDAO;
    @Autowired
    private RepairDAO repairDAO;


    /**
     * 新增【客户管理】
     *
     * @param customerDTO
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public CustomerPO save(CustomerAddDTO customerDTO) {
        CustomerPO customer = CustomerMapper.INSTANCE.fromAddDTO(customerDTO);
        customerDAO.save(customer);
        return customer;
    }

    /**
     * 修改【客户管理】
     *
     * @param customerUpdateDTO
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public CustomerPO update(CustomerUpdateDTO customerUpdateDTO) {
        Long customerId = customerUpdateDTO.getCustomerId();
        CustomerPO customer = this.getCustomer(customerId, true);
        CustomerMapper.INSTANCE.setUpdateDTO(customer, customerUpdateDTO);
        customerDAO.update(customer);
        return customer;
    }

    /**
     * 查询分页列表
     *
     * @param customerQO
     * @return
     */
    public PageVO<CustomerListVO> list(CustomerQO customerQO) {
        PageVO<CustomerListVO> page = customerDAO.findByPage(customerQO);
        return page;
    }

    /**
     * 查询【客户管理】选项列表
     *
     * @return
     */
    public List<OptionVO<Long, String>> findOptions(OptionQO<Long, String> qo) {
        List<OptionVO<Long, String>> options = customerDAO.findOptions(qo);
        return options;
    }

    /**
     * 根据主键获取【客户管理】
     *
     * @param customerId 主键
     * @param force 是否强制获取
     * @return
     */
    public CustomerPO getCustomer(Long customerId, boolean force) {
        CustomerPO customer = customerDAO.findById(customerId);
        if (force && customer == null) {
            throw new BusinessException(ErrorCode.RECORD_NOT_FIND);
        }
        return customer;
    }


    /**
     * 查询【客户管理】详情
     *
     * @param customerId
     * @return
     */
    public CustomerShowVO show(Long customerId) {
        CustomerPO customer = this.getCustomer(customerId, true);
        CustomerShowVO showVO = CustomerMapper.INSTANCE.toShowVO(customer);
        return showVO;
    }

    /**
     * 删除【客户管理】
     *
     * @param customerIds
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public int delete(Long... customerIds) {
        int count = 0;
        for (Long customerId : customerIds) {
            this.checkDeleteByOrder(customerId);
            this.checkDeleteByRepair(customerId);
            count += customerDAO.delete(customerId);
        }
        return count;
    }

    /**
     * 校验是否存在【工单中心】关联
     *
     * @param customerId
     */
    private void checkDeleteByOrder(Long customerId) {
        int count = orderDAO.getCountByCustomerId(customerId);
        if (count > 0) {
            throw new BusinessException(ErrorCode.CASCADE_DELETE_ERROR);
        }
    }

    /**
     * 校验是否存在【报修中心】关联
     *
     * @param customerId
     */
    private void checkDeleteByRepair(Long customerId) {
        int count = repairDAO.getCountByCustomerId(customerId);
        if (count > 0) {
            throw new BusinessException(ErrorCode.CASCADE_DELETE_ERROR);
        }
    }


}


