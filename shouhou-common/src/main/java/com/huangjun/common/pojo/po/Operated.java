package com.huangjun.common.pojo.po;


/**
 * 操作人&操作时间
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface Operated extends OperatedBy, OperatedTime {
}

