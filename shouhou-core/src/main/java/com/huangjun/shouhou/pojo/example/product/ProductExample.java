package com.huangjun.shouhou.pojo.example.product;


/**
 * 【产品管理】参数示例
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class ProductExample {

    public static final String N_PRODUCT_ID = "主键ID";
    public static final String E_PRODUCT_ID = "1";
    public static final String N_NAME = "产品名称";
    public static final String E_NAME = "测试产品";
    public static final String N_NUMBER = "产品编号";
    public static final String E_NUMBER = "XTS201908290021";
    public static final String N_TYPE = "产品分类：具体详细请见 枚举管理查看";
    public static final String E_TYPE = "1";
    public static final String N_CREATED_TIME = "创建时间【yyyy-MM-dd HH:mm:ss】";
    public static final String E_CREATED_TIME = "2017-12-07 00:00:00";
    public static final String N_CREATED_BY = "创建人【最大长度20】";
    public static final String E_CREATED_BY = "admin";
    public static final String N_OPERATED_TIME = "修改时间【yyyy-MM-dd HH:mm:ss】";
    public static final String E_OPERATED_TIME = "2017-12-07 00:00:00";
}

