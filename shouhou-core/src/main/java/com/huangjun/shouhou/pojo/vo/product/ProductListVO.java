package com.huangjun.shouhou.pojo.vo.product;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huangjun.common.constant.JsonFieldConst;
import com.huangjun.common.pojo.vo.AbstractVO;
import com.huangjun.shouhou.constant.ProductType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

import static com.huangjun.shouhou.pojo.example.product.ProductExample.*;

/**
 * 【产品管理】列表展示对象
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(description = "【产品管理】列表展示对象")
public class ProductListVO extends AbstractVO {

    @ApiModelProperty(notes = N_PRODUCT_ID, example = E_PRODUCT_ID)
    private Long productId;

    @ApiModelProperty(notes = N_NAME, example = E_NAME)
    private String name;

    @ApiModelProperty(notes = N_NUMBER, example = E_NUMBER)
    private String number;

    @ApiModelProperty(notes = N_TYPE, example = E_TYPE, allowableValues = ProductType.VALUES_STR)
    private Integer type;

    @ApiModelProperty(notes = N_OPERATED_TIME, example = E_OPERATED_TIME)
    @JsonFormat(pattern = JsonFieldConst.DEFAULT_DATETIME_FORMAT, timezone = "GMT+8")
    private LocalDateTime operatedTime;



}

