package com.huangjun.shouhou.pojo.mapper.order;

import com.huangjun.shouhou.pojo.dto.order.OrderAddDTO;
import com.huangjun.shouhou.pojo.dto.order.OrderExcelDTO;
import com.huangjun.shouhou.pojo.dto.order.OrderUpdateDTO;
import com.huangjun.shouhou.pojo.po.order.OrderPO;
import com.huangjun.shouhou.pojo.vo.order.OrderExcelVO;
import com.huangjun.shouhou.pojo.vo.order.OrderListVO;
import com.huangjun.shouhou.pojo.vo.order.OrderShowVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 【工单中心】映射
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Mapper
public interface OrderMapper {

    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    /**
     * addDTO映射po
     *
     * @param orderAddDTO
     * @return
     */
    OrderPO fromAddDTO(OrderAddDTO orderAddDTO);

    /**
     * 将updateDTO中的值设置到po
     *
     * @param orderPO
     * @param orderUpdateDTO
     */
    void setUpdateDTO(@MappingTarget OrderPO orderPO, OrderUpdateDTO orderUpdateDTO);

    /**
     * po映射showVO
     *
     * @param orderPO
     * @return
     */
    OrderShowVO toShowVO(OrderPO orderPO);


    /**
     * excelDTO映射addDTO
     *
     * @param dto
     * @return
     */
    @Mappings({
            @Mapping(target = "serverType", expression = "java(com.huangjun.shouhou.constant.ServerType.descToValue(dto.getServerType()))"),
            @Mapping(target = "status", expression = "java(com.huangjun.shouhou.constant.OrderStatus.descToValue(dto.getStatus()))"),
    })
    OrderAddDTO fromExcelDTO(OrderExcelDTO dto);

    /**
     * listVO列表转excelVO列表
     *
     * @param list
     * @return
     */
    List<OrderExcelVO> toExcelVOList(List<OrderListVO> list);

    /**
     * listVO转excelVO
     *
     * @param vo
     * @return
     */
    @Mappings({
            @Mapping(target = "serverType", expression = "java(com.huangjun.shouhou.constant.ServerType.valueToDesc(vo.getServerType()))"),
            @Mapping(target = "status", expression = "java(com.huangjun.shouhou.constant.OrderStatus.valueToDesc(vo.getStatus()))"),
    })
    OrderExcelVO toExcelVO(OrderListVO vo);


}

