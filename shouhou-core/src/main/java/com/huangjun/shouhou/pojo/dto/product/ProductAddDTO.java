package com.huangjun.shouhou.pojo.dto.product;

import com.huangjun.common.pojo.dto.AbstractDTO;
import com.huangjun.common.validator.Const;
import com.huangjun.shouhou.constant.ProductType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import static com.huangjun.shouhou.pojo.example.product.ProductExample.*;

/**
 * 新增【产品管理】的参数
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(description = "新增【产品管理】的参数")
public class ProductAddDTO extends AbstractDTO {

    @ApiModelProperty(notes = N_NAME, example = E_NAME)
    @Length(max = 50)
    private String name;

    @ApiModelProperty(notes = N_NUMBER, example = E_NUMBER)
    @Length(max = 50)
    private String number;

    @ApiModelProperty(notes = N_TYPE, example = E_TYPE, allowableValues = ProductType.VALUES_STR)
    @Const(constClass = ProductType.class)
    private Integer type;


}


