package com.huangjun.shouhou.service.order;

import com.huangjun.common.constant.ErrorCode;
import com.huangjun.common.exception.BusinessException;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.dao.order.CustomerDAO;
import com.huangjun.shouhou.dao.order.OrderDAO;
import com.huangjun.shouhou.dao.order.ProductDAO;
import com.huangjun.shouhou.dao.order.UserDAO;
import com.huangjun.shouhou.pojo.dto.order.OrderAddDTO;
import com.huangjun.shouhou.pojo.dto.order.OrderUpdateDTO;
import com.huangjun.shouhou.pojo.mapper.order.OrderMapper;
import com.huangjun.shouhou.pojo.po.order.CustomerPO;
import com.huangjun.shouhou.pojo.po.order.OrderPO;
import com.huangjun.shouhou.pojo.po.order.ProductPO;
import com.huangjun.shouhou.pojo.po.order.UserPO;
import com.huangjun.shouhou.pojo.qo.order.OrderQO;
import com.huangjun.shouhou.pojo.vo.order.OrderListVO;
import com.huangjun.shouhou.pojo.vo.order.OrderShowVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * 【工单中心】增删改查服务
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Service
public class OrderService {

    @Autowired
    private CustomerDAO customerDAO;
    @Autowired
    private OrderDAO orderDAO;
    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private UserDAO userDAO;


    /**
     * 新增【工单中心】
     *
     * @param orderDTO
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public OrderPO save(OrderAddDTO orderDTO) {
        OrderPO order = OrderMapper.INSTANCE.fromAddDTO(orderDTO);
        if (order.getCustomerId() != null) {
            Assert.isTrue(customerDAO.exist(order.getCustomerId()), "客户有误");
        }
        if (order.getProductId() != null) {
            Assert.isTrue(productDAO.exist(order.getProductId()), "客户产品有误");
        }
        if (order.getUserId() != null) {
            Assert.isTrue(userDAO.exist(order.getUserId()), "选择维修工有误");
        }
        orderDAO.save(order);
        return order;
    }

    /**
     * 批量新增【工单中心】
     *
     * @param list
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public int batchSave(List<OrderAddDTO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return 0;
        }
        for (OrderAddDTO addDTO : list) {
            this.save(addDTO);
        }
        return list.size();
    }

    /**
     * 修改【工单中心】
     *
     * @param orderUpdateDTO
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public OrderPO update(OrderUpdateDTO orderUpdateDTO) {
        Long orderId = orderUpdateDTO.getOrderId();
        OrderPO order = this.getOrder(orderId, true);
        OrderMapper.INSTANCE.setUpdateDTO(order, orderUpdateDTO);
        if (order.getCustomerId() != null) {
            Assert.isTrue(customerDAO.exist(order.getCustomerId()), "客户有误");
        }
        if (order.getProductId() != null) {
            Assert.isTrue(productDAO.exist(order.getProductId()), "客户产品有误");
        }
        if (order.getUserId() != null) {
            Assert.isTrue(userDAO.exist(order.getUserId()), "选择维修工有误");
        }
        orderDAO.update(order);
        return order;
    }

    /**
     * 查询分页列表
     *
     * @param orderQO
     * @return
     */
    public PageVO<OrderListVO> list(OrderQO orderQO) {
        PageVO<OrderListVO> page = orderDAO.findByPage(orderQO);
        return page;
    }

    /**
     * 根据主键获取【工单中心】
     *
     * @param orderId 主键
     * @param force 是否强制获取
     * @return
     */
    public OrderPO getOrder(Long orderId, boolean force) {
        OrderPO order = orderDAO.findById(orderId);
        if (force && order == null) {
            throw new BusinessException(ErrorCode.RECORD_NOT_FIND);
        }
        return order;
    }


    /**
     * 查询【工单中心】详情
     *
     * @param orderId
     * @return
     */
    public OrderShowVO show(Long orderId) {
        OrderPO order = this.getOrder(orderId, true);
        OrderShowVO showVO = OrderMapper.INSTANCE.toShowVO(order);
        if (order.getCustomerId() != null) {
            CustomerPO _customerPO = customerDAO.findById(order.getCustomerId());
            showVO.setCustomerName(_customerPO.getName());
        }
        if (order.getProductId() != null) {
            ProductPO _productPO = productDAO.findById(order.getProductId());
            showVO.setProductName(_productPO.getName());
        }
        if (order.getUserId() != null) {
            UserPO _userPO = userDAO.findById(order.getUserId());
            showVO.setUserName(_userPO.getName());
        }
        return showVO;
    }

    /**
     * 删除【工单中心】
     *
     * @param orderIds
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public int delete(Long... orderIds) {
        int count = 0;
        for (Long orderId : orderIds) {
            count += orderDAO.delete(orderId);
        }
        return count;
    }


}


