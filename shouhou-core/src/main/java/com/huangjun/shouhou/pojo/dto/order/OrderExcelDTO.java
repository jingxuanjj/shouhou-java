package com.huangjun.shouhou.pojo.dto.order;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.huangjun.common.util.SafeUtil;
import com.huangjun.shouhou.constant.OrderStatus;
import com.huangjun.shouhou.constant.ServerType;
import com.huangjun.shouhou.pojo.dto.AbstractExcelDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

import static com.huangjun.shouhou.pojo.example.order.OrderExample.*;

/**
 * excel导入【工单中心】的数据传输对象
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class OrderExcelDTO extends AbstractExcelDTO {

    @ExcelProperty("关联报修单号")
    @ColumnWidth(15)
    private String repairNumber;

    @ExcelProperty("工单编号")
    @ColumnWidth(15)
    private String orderNumber;

    @ExcelProperty("客户联系人")
    @ColumnWidth(15)
    private String customerContacts;

    @ExcelProperty("电话")
    @ColumnWidth(15)
    private String phone;

    @ExcelProperty("地址")
    @ColumnWidth(15)
    private String address;

    @ExcelProperty("服务类型*")
    @ColumnWidth(15)
    private String serverType;

    @ExcelProperty("本次服务费")
    @ColumnWidth(15)
    private BigDecimal money;

    @ExcelProperty("备注")
    @ColumnWidth(15)
    private String remark;

    @ExcelProperty("上传附件")
    @ColumnWidth(15)
    private String filePath;

    @ExcelProperty("工单状态")
    @ColumnWidth(15)
    private String status;

    @ExcelProperty("客户")
    @ColumnWidth(15)
    private Long customerId;

    @ExcelProperty("客户产品")
    @ColumnWidth(15)
    private Long productId;

    @ExcelProperty("选择维修工")
    @ColumnWidth(15)
    private Long userId;


    /**
     * 创建模板示例
     *
     * @return
     */
    public static OrderExcelDTO example() {
        OrderExcelDTO example = new OrderExcelDTO();
        example.setRepairNumber(E_REPAIR_NUMBER);
        example.setOrderNumber(E_ORDER_NUMBER);
        example.setCustomerContacts(E_CUSTOMER_CONTACTS);
        example.setPhone(E_PHONE);
        example.setAddress(E_ADDRESS);
        example.setServerType(ServerType.valueToDesc(SafeUtil.getInteger(E_SERVER_TYPE)));
        example.setMoney(SafeUtil.getBigDecimal(E_MONEY));
        example.setRemark(E_REMARK);
        example.setFilePath(E_FILE_PATH);
        example.setStatus(OrderStatus.valueToDesc(SafeUtil.getInteger(E_STATUS)));
        example.setCustomerId(SafeUtil.getLong(E_CUSTOMER_ID));
        example.setProductId(SafeUtil.getLong(E_PRODUCT_ID));
        example.setUserId(SafeUtil.getLong(E_USER_ID));
        return example;
    }

}


