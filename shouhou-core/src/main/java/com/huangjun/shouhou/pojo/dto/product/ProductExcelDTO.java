package com.huangjun.shouhou.pojo.dto.product;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.huangjun.common.util.SafeUtil;
import com.huangjun.shouhou.constant.ProductType;
import com.huangjun.shouhou.pojo.dto.AbstractExcelDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import static com.huangjun.shouhou.pojo.example.product.ProductExample.*;

/**
 * excel导入【产品管理】的数据传输对象
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class ProductExcelDTO extends AbstractExcelDTO {

    @ExcelProperty("产品名称")
    @ColumnWidth(15)
    private String name;

    @ExcelProperty("产品编号")
    @ColumnWidth(15)
    private String number;

    @ExcelProperty("产品分类")
    @ColumnWidth(15)
    private String type;


    /**
     * 创建模板示例
     *
     * @return
     */
    public static ProductExcelDTO example() {
        ProductExcelDTO example = new ProductExcelDTO();
        example.setName(E_NAME);
        example.setNumber(E_NUMBER);
        example.setType(ProductType.valueToDesc(SafeUtil.getInteger(E_TYPE)));
        return example;
    }

}


