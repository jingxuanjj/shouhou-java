package com.huangjun.shouhou.pojo.example.order;


/**
 * 【工单中心】参数示例
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class OrderExample {

    public static final String N_ORDER_ID = "主键ID";
    public static final String E_ORDER_ID = "1";
    public static final String N_REPAIR_NUMBER = "关联报修单号";
    public static final String E_REPAIR_NUMBER = "XTC021198416001";
    public static final String N_ORDER_NUMBER = "工单编号";
    public static final String E_ORDER_NUMBER = "CJK02190800001";
    public static final String N_CUSTOMER_CONTACTS = "客户联系人";
    public static final String E_CUSTOMER_CONTACTS = "李总";
    public static final String N_PHONE = "电话";
    public static final String E_PHONE = "13734567890";
    public static final String N_ADDRESS = "地址";
    public static final String E_ADDRESS = "四川省成都市高新区天府大道125号";
    public static final String N_SERVER_TYPE = "服务类型";
    public static final String E_SERVER_TYPE = "1";
    public static final String N_MONEY = "本次服务费";
    public static final String E_MONEY = "150.00";
    public static final String N_REMARK = "备注";
    public static final String E_REMARK = "备注";
    public static final String N_FILE_PATH = "上传附件";
    public static final String E_FILE_PATH = "上传附件";
    public static final String N_STATUS = "工单状态";
    public static final String E_STATUS = "1";
    public static final String N_CUSTOMER_ID = "客户";
    public static final String E_CUSTOMER_ID = "1";
    public static final String N_PRODUCT_ID = "客户产品";
    public static final String E_PRODUCT_ID = "1";
    public static final String N_USER_ID = "选择维修工";
    public static final String E_USER_ID = "1";
    public static final String N_CREATED_TIME = "创建时间【yyyy-MM-dd HH:mm:ss】";
    public static final String E_CREATED_TIME = "2017-12-07 00:00:00";
    public static final String N_OPERATED_TIME = "修改时间【yyyy-MM-dd HH:mm:ss】";
    public static final String E_OPERATED_TIME = "2017-12-07 00:00:00";
}

