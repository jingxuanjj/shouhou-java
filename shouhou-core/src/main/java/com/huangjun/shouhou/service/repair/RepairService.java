package com.huangjun.shouhou.service.repair;

import com.huangjun.common.constant.ErrorCode;
import com.huangjun.common.exception.BusinessException;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.dao.repair.CustomerDAO;
import com.huangjun.shouhou.dao.repair.ProductDAO;
import com.huangjun.shouhou.dao.repair.RepairDAO;
import com.huangjun.shouhou.dao.repair.UserDAO;
import com.huangjun.shouhou.pojo.dto.repair.RepairAddDTO;
import com.huangjun.shouhou.pojo.dto.repair.RepairUpdateDTO;
import com.huangjun.shouhou.pojo.mapper.repair.RepairMapper;
import com.huangjun.shouhou.pojo.po.repair.CustomerPO;
import com.huangjun.shouhou.pojo.po.repair.ProductPO;
import com.huangjun.shouhou.pojo.po.repair.RepairPO;
import com.huangjun.shouhou.pojo.po.repair.UserPO;
import com.huangjun.shouhou.pojo.qo.repair.RepairQO;
import com.huangjun.shouhou.pojo.vo.repair.RepairListVO;
import com.huangjun.shouhou.pojo.vo.repair.RepairShowVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * 【报修中心】增删改查服务
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Service
public class RepairService {

    @Autowired
    private CustomerDAO customerDAO;
    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private RepairDAO repairDAO;
    @Autowired
    private UserDAO userDAO;


    /**
     * 新增【报修中心】
     *
     * @param repairDTO
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public RepairPO save(RepairAddDTO repairDTO) {
        RepairPO repair = RepairMapper.INSTANCE.fromAddDTO(repairDTO);
        if (repair.getCustomerId() != null) {
            Assert.isTrue(customerDAO.exist(repair.getCustomerId()), "客户有误");
        }
        if (repair.getProductId() != null) {
            Assert.isTrue(productDAO.exist(repair.getProductId()), "客户产品有误");
        }
        if (repair.getUserId() != null) {
            Assert.isTrue(userDAO.exist(repair.getUserId()), "选择维修工有误");
        }
        repairDAO.save(repair);
        return repair;
    }

    /**
     * 批量新增【报修中心】
     *
     * @param list
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public int batchSave(List<RepairAddDTO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return 0;
        }
        for (RepairAddDTO addDTO : list) {
            this.save(addDTO);
        }
        return list.size();
    }

    /**
     * 修改【报修中心】
     *
     * @param repairUpdateDTO
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public RepairPO update(RepairUpdateDTO repairUpdateDTO) {
        Long repairId = repairUpdateDTO.getRepairId();
        RepairPO repair = this.getRepair(repairId, true);
        RepairMapper.INSTANCE.setUpdateDTO(repair, repairUpdateDTO);
        if (repair.getCustomerId() != null) {
            Assert.isTrue(customerDAO.exist(repair.getCustomerId()), "客户有误");
        }
        if (repair.getProductId() != null) {
            Assert.isTrue(productDAO.exist(repair.getProductId()), "客户产品有误");
        }
        if (repair.getUserId() != null) {
            Assert.isTrue(userDAO.exist(repair.getUserId()), "选择维修工有误");
        }
        repairDAO.update(repair);
        return repair;
    }

    /**
     * 查询分页列表
     *
     * @param repairQO
     * @return
     */
    public PageVO<RepairListVO> list(RepairQO repairQO) {
        PageVO<RepairListVO> page = repairDAO.findByPage(repairQO);
        return page;
    }

    /**
     * 根据主键获取【报修中心】
     *
     * @param repairId 主键
     * @param force 是否强制获取
     * @return
     */
    public RepairPO getRepair(Long repairId, boolean force) {
        RepairPO repair = repairDAO.findById(repairId);
        if (force && repair == null) {
            throw new BusinessException(ErrorCode.RECORD_NOT_FIND);
        }
        return repair;
    }


    /**
     * 查询【报修中心】详情
     *
     * @param repairId
     * @return
     */
    public RepairShowVO show(Long repairId) {
        RepairPO repair = this.getRepair(repairId, true);
        RepairShowVO showVO = RepairMapper.INSTANCE.toShowVO(repair);
        if (repair.getCustomerId() != null) {
            CustomerPO _customerPO = customerDAO.findById(repair.getCustomerId());
            showVO.setCustomerName(_customerPO.getName());
        }
        if (repair.getProductId() != null) {
            ProductPO _productPO = productDAO.findById(repair.getProductId());
            showVO.setProductName(_productPO.getName());
        }
        if (repair.getUserId() != null) {
            UserPO _userPO = userDAO.findById(repair.getUserId());
            showVO.setUserName(_userPO.getName());
        }
        return showVO;
    }

    /**
     * 删除【报修中心】
     *
     * @param repairIds
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public int delete(Long... repairIds) {
        int count = 0;
        for (Long repairId : repairIds) {
            count += repairDAO.delete(repairId);
        }
        return count;
    }


}


