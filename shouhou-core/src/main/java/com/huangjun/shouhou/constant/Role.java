package com.huangjun.shouhou.constant;

import com.huangjun.common.validator.Check;

import java.util.HashMap;
import java.util.Map;

/**
 * 枚举【员工角色】
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public enum Role {

    /**
     * 超级管理员
     */
    ADMIN(1, "超级管理员"),
    /**
     * 维修工程师
     */
    WEIXIU(2, "维修工程师"),
    /**
     * 客服专员
     */
    KEFU(3, "客服专员"),
    ;


    /**
     * 枚举值罗列，给swagger接口文档展示用
     */
    public static final String VALUES_STR = "1,2,3";

    private static final Map<Integer, Role> LOOKUP = new HashMap<>();

    static {
        for (Role e : Role.values()) {
            LOOKUP.put(e.value, e);
        }
    }

    private final Integer value;
    private final String desc;


    Role(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static Role find(Integer value) {
        return LOOKUP.get(value);
    }

    public static Role findByDesc(String desc) {
        for (Role e : Role.values()) {
            if (e.getDesc().equals(desc)) {
                return e;
            }
        }
        return null;
    }


    /**
     * desc映射value
     *
     * @param desc
     * @return
     */
    public static Integer descToValue(String desc) {
        Role theEnum = findByDesc(desc);
        if (theEnum != null) {
            return theEnum.getValue();
        }
        return null;
    }

    /**
     * value映射desc
     *
     * @param value
     * @return
     */
    public static String valueToDesc(Integer value) {
        Role theEnum = find(value);
        if (theEnum != null) {
            return theEnum.getDesc();
        }
        return null;
    }

    /**
     * 校验有效性
     */
    @Check
    public static final boolean validate(Integer value) {
        Role theEnum = find(value);
        return theEnum != null;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }


}

