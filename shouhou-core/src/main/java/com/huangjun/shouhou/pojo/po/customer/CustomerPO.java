package com.huangjun.shouhou.pojo.po.customer;

import com.huangjun.common.pojo.po.AbstractPO;
import com.huangjun.common.pojo.po.Jsr310Created;
import com.huangjun.common.pojo.po.Jsr310OperatedTime;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 客户管理
 * <p>客户管理
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class CustomerPO extends AbstractPO implements Jsr310Created, Jsr310OperatedTime {

    /**
     * 主键ID
     */
    private Long customerId;

    /**
     * 客户名称
     */
    private String name;

    /**
     * 客户编号
     */
    private String number;

    /**
     * 联系人
     */
    private String phoneName;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 地址
     */
    private String address;

    /**
     * 客户负责人
     */
    private String owner;

    /**
     * 创建时间【yyyy-MM-dd HH:mm:ss】
     */
    private LocalDateTime createdTime;

    /**
     * 创建人【最大长度20】
     */
    private String createdBy;

    /**
     * 修改时间【yyyy-MM-dd HH:mm:ss】
     */
    private LocalDateTime operatedTime;


}

