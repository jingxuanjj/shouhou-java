package com.huangjun.shouhou.pojo.example.user;


/**
 * 【用户表(员工表)】参数示例
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class UserExample {

    public static final String N_USER_ID = "主键ID";
    public static final String E_USER_ID = "1";
    public static final String N_NAME = "姓名";
    public static final String E_NAME = "黄俊";
    public static final String N_USERNAME = "账号";
    public static final String E_USERNAME = "admin";
    public static final String N_PASSWORD = "密码";
    public static final String E_PASSWORD = "123456";
    public static final String N_PHONE = "电话";
    public static final String E_PHONE = "电话";
    public static final String N_ROLE = "角色";
    public static final String E_ROLE = "角色";
    public static final String N_CREATED_TIME = "创建时间【yyyy-MM-dd HH:mm:ss】";
    public static final String E_CREATED_TIME = "2017-12-07 00:00:00";
    public static final String N_CREATED_BY = "创建人【最大长度20】";
    public static final String E_CREATED_BY = "admin";
    public static final String N_OPERATED_TIME = "修改时间【yyyy-MM-dd HH:mm:ss】";
    public static final String E_OPERATED_TIME = "2017-12-07 00:00:00";
}

