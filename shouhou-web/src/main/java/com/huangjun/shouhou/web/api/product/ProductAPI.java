package com.huangjun.shouhou.web.api.product;

import com.huangjun.common.pojo.qo.OptionQO;
import com.huangjun.common.pojo.vo.OptionVO;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.pojo.dto.product.ProductAddDTO;
import com.huangjun.shouhou.pojo.dto.product.ProductUpdateDTO;
import com.huangjun.shouhou.pojo.qo.product.ProductQO;
import com.huangjun.shouhou.pojo.vo.product.ProductListVO;
import com.huangjun.shouhou.pojo.vo.product.ProductShowVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 【产品管理】API
 * <p>swagger接口文档
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Api(tags = "【产品管理】API")
public interface ProductAPI {

    /**
     * 新增【产品管理】
     */
    @ApiOperation(value = "新增【产品管理】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productAddDTO", dataTypeClass = ProductAddDTO.class, value = "新增【产品管理】参数", paramType = "body"),
    })
    ResponseEntity<ProductShowVO> save(ProductAddDTO productAddDTO) throws Exception;

    /**
     * 修改【产品管理】
     */
    @ApiOperation(value = "修改【产品管理】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productUpdateDTO", dataTypeClass = ProductUpdateDTO.class, value = "修改【产品管理】参数", paramType = "body"),
    })
    ResponseEntity<ProductShowVO> update(ProductUpdateDTO productUpdateDTO);

    /**
     * 分页查询【产品管理】
     */
    @ApiOperation(value = "分页查询【产品管理】")
    ResponseEntity<PageVO<ProductListVO>> list(ProductQO productQO);

    /**
     * 查询【产品管理】选项列表
     */
    @ApiOperation(value = "查询【产品管理】选项列表")
    ResponseEntity<List<OptionVO<Long, String>>> findOptions(OptionQO<Long, String> qo);

    /**
     * 查看【产品管理】详情
     */
    @ApiOperation(value = "查看【产品管理】详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", dataTypeClass = Long.class, value = "【产品管理】id", paramType = "path"),
    })
    ResponseEntity<ProductShowVO> show(Long productId);

    /**
     * 删除单个【产品管理】
     */
    @ApiOperation(value = "删除单个【产品管理】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "productId", dataTypeClass = Long.class, value = "【产品管理】id", paramType = "path"),
    })
    ResponseEntity<Integer> delete(Long productId);

    /**
     * 批量删除【产品管理】
     */
    @ApiOperation(value = "批量删除【产品管理】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", dataTypeClass = Long.class, allowMultiple = true, value = "id数组", paramType = "body"),
    })
    ResponseEntity<Integer> deleteBatch(Long[] id);

    /**
     * 导出【产品管理】excel
     */
    @ApiOperation(value = "导出【产品管理】excel")
    void exportExcel(ProductQO productQO, HttpServletResponse response) throws Exception;

    /**
     * 导入【产品管理】excel
     */
    @ApiOperation(value = "导入【产品管理】excel")
    ResponseEntity<Integer> importExcel(MultipartFile file) throws Exception;

    /**
     * 下载【产品管理】excel模板
     */
    @ApiOperation(value = "下载【产品管理】excel模板")
    void downloadExcelTemplate(HttpServletResponse response) throws Exception;

}

