package com.huangjun.common.pojo.po;


/**
 * 逻辑删除+创建人&创建时间+操作人&操作时间+版本号
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface CreatedOperatedDeletedVersion extends Created, Operated, Deleted, Version {
}

