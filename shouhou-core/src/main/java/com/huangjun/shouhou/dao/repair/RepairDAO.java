package com.huangjun.shouhou.dao.repair;

import com.huangjun.common.dao.DAO;
import com.huangjun.shouhou.pojo.po.repair.RepairPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 【报修中心】数据库操作
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Repository
@Mapper
public interface RepairDAO extends DAO<RepairPO> {

    int getCountByCustomerId(Long customerId);

    int getCountByProductId(Long productId);

    int getCountByUserId(Long userId);


}



