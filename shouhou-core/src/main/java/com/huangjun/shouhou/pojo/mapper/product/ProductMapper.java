package com.huangjun.shouhou.pojo.mapper.product;

import com.huangjun.shouhou.pojo.dto.product.ProductAddDTO;
import com.huangjun.shouhou.pojo.dto.product.ProductExcelDTO;
import com.huangjun.shouhou.pojo.dto.product.ProductUpdateDTO;
import com.huangjun.shouhou.pojo.po.product.ProductPO;
import com.huangjun.shouhou.pojo.vo.product.ProductExcelVO;
import com.huangjun.shouhou.pojo.vo.product.ProductListVO;
import com.huangjun.shouhou.pojo.vo.product.ProductShowVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 【产品管理】映射
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Mapper
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    /**
     * addDTO映射po
     *
     * @param productAddDTO
     * @return
     */
    ProductPO fromAddDTO(ProductAddDTO productAddDTO);

    /**
     * 将updateDTO中的值设置到po
     *
     * @param productPO
     * @param productUpdateDTO
     */
    void setUpdateDTO(@MappingTarget ProductPO productPO, ProductUpdateDTO productUpdateDTO);

    /**
     * po映射showVO
     *
     * @param productPO
     * @return
     */
    ProductShowVO toShowVO(ProductPO productPO);


    /**
     * excelDTO映射addDTO
     *
     * @param dto
     * @return
     */
    @Mappings({
            @Mapping(target = "type", expression = "java(com.huangjun.shouhou.constant.ProductType.descToValue(dto.getType()))"),
    })
    ProductAddDTO fromExcelDTO(ProductExcelDTO dto);

    /**
     * listVO列表转excelVO列表
     *
     * @param list
     * @return
     */
    List<ProductExcelVO> toExcelVOList(List<ProductListVO> list);

    /**
     * listVO转excelVO
     *
     * @param vo
     * @return
     */
    @Mappings({
            @Mapping(target = "type", expression = "java(com.huangjun.shouhou.constant.ProductType.valueToDesc(vo.getType()))"),
    })
    ProductExcelVO toExcelVO(ProductListVO vo);


}

