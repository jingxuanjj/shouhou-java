package com.huangjun.shouhou.pojo.po.product;

import com.huangjun.common.pojo.po.AbstractPO;
import com.huangjun.common.pojo.po.Jsr310Created;
import com.huangjun.common.pojo.po.Jsr310OperatedTime;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 产品管理
 * <p>产品管理
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class ProductPO extends AbstractPO implements Jsr310Created, Jsr310OperatedTime {

    /**
     * 主键ID
     */
    private Long productId;

    /**
     * 产品名称
     */
    private String name;

    /**
     * 产品编号
     */
    private String number;

    /**
     * 产品分类：具体详细请见 枚举管理查看
     *
     * @see com.huangjun.shouhou.constant.ProductType
     */
    private Integer type;

    /**
     * 创建时间【yyyy-MM-dd HH:mm:ss】
     */
    private LocalDateTime createdTime;

    /**
     * 创建人【最大长度20】
     */
    private String createdBy;

    /**
     * 修改时间【yyyy-MM-dd HH:mm:ss】
     */
    private LocalDateTime operatedTime;


}

