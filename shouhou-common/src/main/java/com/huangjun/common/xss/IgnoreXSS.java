package com.huangjun.common.xss;

import java.lang.annotation.*;

/**
 * 无视XSS脚本
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface IgnoreXSS {


}

