package com.huangjun.shouhou.web.api.repair;

import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.pojo.dto.repair.RepairAddDTO;
import com.huangjun.shouhou.pojo.dto.repair.RepairUpdateDTO;
import com.huangjun.shouhou.pojo.qo.repair.RepairQO;
import com.huangjun.shouhou.pojo.vo.repair.RepairListVO;
import com.huangjun.shouhou.pojo.vo.repair.RepairShowVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 【报修中心】API
 * <p>swagger接口文档
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Api(tags = "【报修中心】API")
public interface RepairAPI {

    /**
     * 新增【报修中心】
     */
    @ApiOperation(value = "新增【报修中心】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "repairAddDTO", dataTypeClass = RepairAddDTO.class, value = "新增【报修中心】参数", paramType = "body"),
    })
    ResponseEntity<RepairShowVO> save(RepairAddDTO repairAddDTO) throws Exception;

    /**
     * 修改【报修中心】
     */
    @ApiOperation(value = "修改【报修中心】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "repairUpdateDTO", dataTypeClass = RepairUpdateDTO.class, value = "修改【报修中心】参数", paramType = "body"),
    })
    ResponseEntity<RepairShowVO> update(RepairUpdateDTO repairUpdateDTO);

    /**
     * 分页查询【报修中心】
     */
    @ApiOperation(value = "分页查询【报修中心】")
    ResponseEntity<PageVO<RepairListVO>> list(RepairQO repairQO);

    /**
     * 查看【报修中心】详情
     */
    @ApiOperation(value = "查看【报修中心】详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "repairId", dataTypeClass = Long.class, value = "【报修中心】id", paramType = "path"),
    })
    ResponseEntity<RepairShowVO> show(Long repairId);

    /**
     * 删除单个【报修中心】
     */
    @ApiOperation(value = "删除单个【报修中心】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "repairId", dataTypeClass = Long.class, value = "【报修中心】id", paramType = "path"),
    })
    ResponseEntity<Integer> delete(Long repairId);

    /**
     * 批量删除【报修中心】
     */
    @ApiOperation(value = "批量删除【报修中心】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", dataTypeClass = Long.class, allowMultiple = true, value = "id数组", paramType = "body"),
    })
    ResponseEntity<Integer> deleteBatch(Long[] id);

    /**
     * 导出【报修中心】excel
     */
    @ApiOperation(value = "导出【报修中心】excel")
    void exportExcel(RepairQO repairQO, HttpServletResponse response) throws Exception;

    /**
     * 导入【报修中心】excel
     */
    @ApiOperation(value = "导入【报修中心】excel")
    ResponseEntity<Integer> importExcel(MultipartFile file) throws Exception;

    /**
     * 下载【报修中心】excel模板
     */
    @ApiOperation(value = "下载【报修中心】excel模板")
    void downloadExcelTemplate(HttpServletResponse response) throws Exception;

}

