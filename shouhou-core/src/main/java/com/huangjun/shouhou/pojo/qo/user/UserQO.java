package com.huangjun.shouhou.pojo.qo.user;

import com.huangjun.common.pojo.qo.PageQO;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import static com.huangjun.shouhou.pojo.example.user.UserExample.*;

/**
 * 查询【用户表(员工表)】的参数
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class UserQO extends PageQO {

    @ApiParam(value = N_NAME, example = E_NAME)
    @Length(max = 32, message = "name最大长度不能超过{max}")
    private String name;

    @ApiParam(value = N_ROLE, example = E_ROLE)
    private Integer role;


}

