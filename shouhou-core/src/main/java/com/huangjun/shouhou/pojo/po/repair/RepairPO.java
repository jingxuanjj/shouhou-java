package com.huangjun.shouhou.pojo.po.repair;

import com.huangjun.common.pojo.po.AbstractPO;
import com.huangjun.common.pojo.po.Jsr310Created;
import com.huangjun.common.pojo.po.Jsr310OperatedTime;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 报修中心
 * <p>报修中心
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class RepairPO extends AbstractPO implements Jsr310Created, Jsr310OperatedTime {

    /**
     * 主键ID
     */
    private Long repairId;

    /**
     * 报修单编号
     */
    private String repairNumber;

    /**
     * 客户联系人
     */
    private String customerContacts;

    /**
     * 电话
     */
    private String phone;

    /**
     * 地址
     */
    private String address;

    /**
     * 上传附件
     */
    private String filePath;

    /**
     * 报修类型
     *
     * @see com.huangjun.shouhou.constant.RepairType
     */
    private Integer repairType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 工单状态
     *
     * @see com.huangjun.shouhou.constant.RepairStatus
     */
    private Integer status;

    /**
     * 客户
     */
    private Long customerId;

    /**
     * 客户产品
     */
    private Long productId;

    /**
     * 选择维修工
     */
    private Long userId;

    /**
     * 创建时间【yyyy-MM-dd HH:mm:ss】
     */
    private LocalDateTime createdTime;

    /**
     * 创建人【最大长度20】
     */
    private String createdBy;

    /**
     * 修改时间【yyyy-MM-dd HH:mm:ss】
     */
    private LocalDateTime operatedTime;


}

