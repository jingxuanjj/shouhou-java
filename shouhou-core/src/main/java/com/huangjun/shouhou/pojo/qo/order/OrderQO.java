package com.huangjun.shouhou.pojo.qo.order;

import com.huangjun.common.pojo.qo.PageQO;
import com.huangjun.shouhou.pojo.example.order.CustomerExample;
import com.huangjun.shouhou.pojo.example.order.ProductExample;
import com.huangjun.shouhou.pojo.example.order.UserExample;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import static com.huangjun.shouhou.pojo.example.order.OrderExample.*;

/**
 * 查询【工单中心】的参数
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class OrderQO extends PageQO {

    @ApiParam(value = N_ORDER_NUMBER, example = E_ORDER_NUMBER)
    @Length(max = 50, message = "orderNumber最大长度不能超过{max}")
    private String orderNumber;

    @ApiParam(value = N_STATUS, example = E_STATUS)
    private Integer status;

    @ApiParam(value = N_PRODUCT_ID, example = E_PRODUCT_ID)
    private Long productId;

    @ApiParam(value = CustomerExample.N_NAME, example = CustomerExample.E_NAME)
    @Length(max = 50, message = "customerName最大长度不能超过{max}")
    private String customerName;

    @ApiParam(value = ProductExample.N_NAME, example = ProductExample.E_NAME)
    @Length(max = 50, message = "productName最大长度不能超过{max}")
    private String productName;

    @ApiParam(value = UserExample.N_NAME, example = UserExample.E_NAME)
    @Length(max = 32, message = "userName最大长度不能超过{max}")
    private String userName;

    @ApiParam(value = "创建时间排序标识【1升序,-1降序,0不排序】", example = "1")
    private Integer createdTimeSortSign;

    @ApiParam(value = "修改时间排序标识【1升序,-1降序,0不排序】", example = "1")
    private Integer operatedTimeSortSign;


}

