package com.huangjun.shouhou.constant;

import com.huangjun.common.validator.Check;

import java.util.HashMap;
import java.util.Map;

/**
 * 枚举【产品分类】
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public enum ProductType {

    /**
     * 配件类
     */
    PEIJIAN(1, "配件类"),
    /**
     * 工具类
     */
    GONGJU(2, "工具类"),
    /**
     * 设备类
     */
    SHEIBEI(3, "设备类"),
    ;


    /**
     * 枚举值罗列，给swagger接口文档展示用
     */
    public static final String VALUES_STR = "1,2,3";

    private static final Map<Integer, ProductType> LOOKUP = new HashMap<>();

    static {
        for (ProductType e : ProductType.values()) {
            LOOKUP.put(e.value, e);
        }
    }

    private final Integer value;
    private final String desc;


    ProductType(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static ProductType find(Integer value) {
        return LOOKUP.get(value);
    }

    public static ProductType findByDesc(String desc) {
        for (ProductType e : ProductType.values()) {
            if (e.getDesc().equals(desc)) {
                return e;
            }
        }
        return null;
    }


    /**
     * desc映射value
     *
     * @param desc
     * @return
     */
    public static Integer descToValue(String desc) {
        ProductType theEnum = findByDesc(desc);
        if (theEnum != null) {
            return theEnum.getValue();
        }
        return null;
    }

    /**
     * value映射desc
     *
     * @param value
     * @return
     */
    public static String valueToDesc(Integer value) {
        ProductType theEnum = find(value);
        if (theEnum != null) {
            return theEnum.getDesc();
        }
        return null;
    }

    /**
     * 校验有效性
     */
    @Check
    public static final boolean validate(Integer value) {
        ProductType theEnum = find(value);
        return theEnum != null;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }


}

