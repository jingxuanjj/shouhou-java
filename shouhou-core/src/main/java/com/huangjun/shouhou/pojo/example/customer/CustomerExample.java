package com.huangjun.shouhou.pojo.example.customer;


/**
 * 【客户管理】参数示例
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class CustomerExample {

    public static final String N_CUSTOMER_ID = "主键ID";
    public static final String E_CUSTOMER_ID = "1";
    public static final String N_NAME = "客户名称";
    public static final String E_NAME = "客户名称";
    public static final String N_NUMBER = "客户编号";
    public static final String E_NUMBER = "CJK02190800001";
    public static final String N_PHONE_NAME = "联系人";
    public static final String E_PHONE_NAME = "张三";
    public static final String N_PHONE = "联系电话";
    public static final String E_PHONE = "13734567890";
    public static final String N_ADDRESS = "地址";
    public static final String E_ADDRESS = "四川省成都市高新区";
    public static final String N_OWNER = "客户负责人";
    public static final String E_OWNER = "李师傅";
    public static final String N_CREATED_TIME = "创建时间【yyyy-MM-dd HH:mm:ss】";
    public static final String E_CREATED_TIME = "2017-12-07 00:00:00";
    public static final String N_CREATED_BY = "创建人【最大长度20】";
    public static final String E_CREATED_BY = "admin";
    public static final String N_OPERATED_TIME = "修改时间【yyyy-MM-dd HH:mm:ss】";
    public static final String E_OPERATED_TIME = "2017-12-07 00:00:00";
}

