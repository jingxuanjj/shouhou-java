package com.huangjun.shouhou.web.constant;


/**
 * web常量
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class WebConst {

    /**
     * 接口路径前缀
     */
    public static final String API_PATH = "/api";

    /**
     * 各个模块的路径
     */
    public static class ModulePath {

        public static final String CUSTOMER = API_PATH + "/customer";

        public static final String PRODUCT = API_PATH + "/product";

        public static final String USER = API_PATH + "/user";

        public static final String ORDER = API_PATH + "/order";

        public static final String REPAIR = API_PATH + "/repair";

    }

}

