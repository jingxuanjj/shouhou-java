package com.huangjun.shouhou;

import com.huangjun.shouhou.web.rest.customer.CustomerControllerTest;
import com.huangjun.shouhou.web.rest.order.OrderControllerTest;
import com.huangjun.shouhou.web.rest.product.ProductControllerTest;
import com.huangjun.shouhou.web.rest.repair.RepairControllerTest;
import com.huangjun.shouhou.web.rest.user.UserControllerTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * 合并测试类
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        CustomerControllerTest.class,
        ProductControllerTest.class,
        UserControllerTest.class,
        OrderControllerTest.class,
        RepairControllerTest.class,
})
public class Main {


}

