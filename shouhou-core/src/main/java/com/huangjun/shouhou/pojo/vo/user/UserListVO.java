package com.huangjun.shouhou.pojo.vo.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.huangjun.common.constant.JsonFieldConst;
import com.huangjun.common.pojo.vo.AbstractVO;
import com.huangjun.shouhou.constant.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

import static com.huangjun.shouhou.pojo.example.user.UserExample.*;

/**
 * 【用户表(员工表)】列表展示对象
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(description = "【用户表(员工表)】列表展示对象")
public class UserListVO extends AbstractVO {

    @ApiModelProperty(notes = N_USER_ID, example = E_USER_ID)
    private Long userId;

    @ApiModelProperty(notes = N_NAME, example = E_NAME)
    private String name;

    @ApiModelProperty(notes = N_PHONE, example = E_PHONE)
    private String phone;

    @ApiModelProperty(notes = N_ROLE, example = E_ROLE, allowableValues = Role.VALUES_STR)
    private Integer role;

    @ApiModelProperty(notes = N_CREATED_TIME, example = E_CREATED_TIME)
    @JsonFormat(pattern = JsonFieldConst.DEFAULT_DATETIME_FORMAT, timezone = "GMT+8")
    private LocalDateTime createdTime;



}

