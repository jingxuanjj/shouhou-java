package com.huangjun.shouhou.pojo.vo.product;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.huangjun.common.pojo.vo.AbstractVO;
import com.huangjun.shouhou.excel.converter.LocalDateTimeConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 【产品管理】excel导出对象
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class ProductExcelVO extends AbstractVO {

    @ExcelProperty("主键")
    @ColumnWidth(12)
    private Long productId;

    @ExcelProperty("产品名称")
    @ColumnWidth(15)
    private String name;

    @ExcelProperty("产品编号")
    @ColumnWidth(15)
    private String number;

    @ExcelProperty("产品分类")
    @ColumnWidth(15)
    private String type;

    @ExcelProperty(value = "修改时间", converter = LocalDateTimeConverter.class)
    @ColumnWidth(25)
    private LocalDateTime operatedTime;



}

