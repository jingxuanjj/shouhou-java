package com.huangjun.shouhou;

import com.huangjun.common.optimistic.EnableOptimisticLock;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * 启动类
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@SpringBootApplication
@EnableOptimisticLock
public class ShouhouApp extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ShouhouApp.class);
        app.run(args);
    }

    /**
     * 兼容tomcat部署模式
     *
     * @param application
     * @return
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ShouhouApp.class);
    }
}

