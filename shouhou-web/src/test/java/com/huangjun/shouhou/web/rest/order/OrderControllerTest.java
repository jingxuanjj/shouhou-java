package com.huangjun.shouhou.web.rest.order;

import com.google.common.collect.Lists;
import com.huangjun.common.util.JsonUtil;
import com.huangjun.shouhou.help.order.OrderHelper;
import com.huangjun.shouhou.pojo.dto.order.OrderAddDTO;
import com.huangjun.shouhou.pojo.dto.order.OrderUpdateDTO;
import com.huangjun.shouhou.pojo.po.order.OrderPO;
import com.huangjun.shouhou.web.AbstractWebTest;
import com.huangjun.shouhou.web.constant.WebConst;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * 【工单中心】单元测试
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class OrderControllerTest extends AbstractWebTest {

    @Autowired
    private OrderHelper orderHelper;


    /**
     * 新增【工单中心】
     */
    @Test
    public void save() throws Exception {
        OrderAddDTO addDTO = orderHelper.getOrderAddDTO(null, null, null);
        restMockMvc.perform(post(WebConst.ModulePath.ORDER + "/order")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(addDTO)))
                .andExpect(status().isCreated());
    }

    /**
     * 修改【工单中心】
     */
    @Test
    public void update() throws Exception {
        OrderPO order = orderHelper.saveOrderExample(null, null, null);
        OrderUpdateDTO updateDTO = orderHelper.getOrderUpdateDTO(order);
        restMockMvc.perform(put(WebConst.ModulePath.ORDER + "/order")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(updateDTO)))
                .andExpect(status().isOk());
    }

    /**
     * 分页查询【工单中心】
     */
    @Test
    public void list() throws Exception {
        OrderPO order = orderHelper.saveOrderExample(null, null, null);
        restMockMvc.perform(get(WebConst.ModulePath.ORDER + "/order"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.list.length()").value(is(1)));
    }

    /**
     * 查看【工单中心】详情
     */
    @Test
    public void show() throws Exception {
        OrderPO order = orderHelper.saveOrderExample(null, null, null);
        restMockMvc.perform(get(WebConst.ModulePath.ORDER + "/order/{orderId}", order.getOrderId()))
                .andExpect(status().isOk());
    }

    /**
     * 删除单个【工单中心】
     */
    @Test
    public void del() throws Exception {
        OrderPO order = orderHelper.saveOrderExample(null, null, null);
        restMockMvc.perform(delete(WebConst.ModulePath.ORDER + "/order/{orderId}", order.getOrderId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }

    /**
     * 批量删除【工单中心】
     */
    @Test
    public void deleteBatch() throws Exception {
        OrderPO order = orderHelper.saveOrderExample(null, null, null);
        restMockMvc.perform(delete(WebConst.ModulePath.ORDER + "/order")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(Lists.newArrayList(order.getOrderId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }

    /**
     * 导入【工单中心】excel
     */
    @Test
    public void importExcel() throws Exception {
        // 首先下载excel模板
        MvcResult mvcResult = restMockMvc.perform(get(WebConst.ModulePath.ORDER + "/order/template"))
                .andExpect(status().isOk())
                .andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();

        // 将模板原封不动导入
        MockMultipartFile file = new MockMultipartFile("file", response.getContentAsByteArray());
        restMockMvc.perform(multipart(WebConst.ModulePath.ORDER + "/order/import")
                .file(file))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }


}
