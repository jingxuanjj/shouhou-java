package com.huangjun.common.pojo.po;


/**
 * 是否乐观锁版本接口
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface Version {

    Integer getVersion();

    void setVersion(Integer version);

}

