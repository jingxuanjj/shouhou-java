package com.huangjun.shouhou.constant;

import com.huangjun.common.validator.Check;

import java.util.HashMap;
import java.util.Map;

/**
 * 枚举【报修单类型】
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public enum RepairType {

    /**
     * 客户问题反馈
     */
    WENTIFANKUI(1, "客户问题反馈"),
    /**
     * 服务请求
     */
    FUWUQINGQIU(2, "服务请求"),
    ;


    /**
     * 枚举值罗列，给swagger接口文档展示用
     */
    public static final String VALUES_STR = "1,2";

    private static final Map<Integer, RepairType> LOOKUP = new HashMap<>();

    static {
        for (RepairType e : RepairType.values()) {
            LOOKUP.put(e.value, e);
        }
    }

    private final Integer value;
    private final String desc;


    RepairType(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static RepairType find(Integer value) {
        return LOOKUP.get(value);
    }

    public static RepairType findByDesc(String desc) {
        for (RepairType e : RepairType.values()) {
            if (e.getDesc().equals(desc)) {
                return e;
            }
        }
        return null;
    }


    /**
     * desc映射value
     *
     * @param desc
     * @return
     */
    public static Integer descToValue(String desc) {
        RepairType theEnum = findByDesc(desc);
        if (theEnum != null) {
            return theEnum.getValue();
        }
        return null;
    }

    /**
     * value映射desc
     *
     * @param value
     * @return
     */
    public static String valueToDesc(Integer value) {
        RepairType theEnum = find(value);
        if (theEnum != null) {
            return theEnum.getDesc();
        }
        return null;
    }

    /**
     * 校验有效性
     */
    @Check
    public static final boolean validate(Integer value) {
        RepairType theEnum = find(value);
        return theEnum != null;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }


}

