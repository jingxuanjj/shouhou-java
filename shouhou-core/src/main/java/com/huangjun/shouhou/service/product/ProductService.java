package com.huangjun.shouhou.service.product;

import com.huangjun.common.constant.ErrorCode;
import com.huangjun.common.exception.BusinessException;
import com.huangjun.common.pojo.qo.OptionQO;
import com.huangjun.common.pojo.vo.OptionVO;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.dao.product.OrderDAO;
import com.huangjun.shouhou.dao.product.ProductDAO;
import com.huangjun.shouhou.dao.product.RepairDAO;
import com.huangjun.shouhou.pojo.dto.product.ProductAddDTO;
import com.huangjun.shouhou.pojo.dto.product.ProductUpdateDTO;
import com.huangjun.shouhou.pojo.mapper.product.ProductMapper;
import com.huangjun.shouhou.pojo.po.product.ProductPO;
import com.huangjun.shouhou.pojo.qo.product.ProductQO;
import com.huangjun.shouhou.pojo.vo.product.ProductListVO;
import com.huangjun.shouhou.pojo.vo.product.ProductShowVO;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 【产品管理】增删改查服务
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Service
public class ProductService {

    @Autowired
    private OrderDAO orderDAO;
    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private RepairDAO repairDAO;


    /**
     * 新增【产品管理】
     *
     * @param productDTO
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public ProductPO save(ProductAddDTO productDTO) {
        ProductPO product = ProductMapper.INSTANCE.fromAddDTO(productDTO);
        productDAO.save(product);
        return product;
    }

    /**
     * 批量新增【产品管理】
     *
     * @param list
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public int batchSave(List<ProductAddDTO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return 0;
        }
        for (ProductAddDTO addDTO : list) {
            this.save(addDTO);
        }
        return list.size();
    }

    /**
     * 修改【产品管理】
     *
     * @param productUpdateDTO
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public ProductPO update(ProductUpdateDTO productUpdateDTO) {
        Long productId = productUpdateDTO.getProductId();
        ProductPO product = this.getProduct(productId, true);
        ProductMapper.INSTANCE.setUpdateDTO(product, productUpdateDTO);
        productDAO.update(product);
        return product;
    }

    /**
     * 查询分页列表
     *
     * @param productQO
     * @return
     */
    public PageVO<ProductListVO> list(ProductQO productQO) {
        PageVO<ProductListVO> page = productDAO.findByPage(productQO);
        return page;
    }

    /**
     * 查询【产品管理】选项列表
     *
     * @return
     */
    public List<OptionVO<Long, String>> findOptions(OptionQO<Long, String> qo) {
        List<OptionVO<Long, String>> options = productDAO.findOptions(qo);
        return options;
    }

    /**
     * 根据主键获取【产品管理】
     *
     * @param productId 主键
     * @param force 是否强制获取
     * @return
     */
    public ProductPO getProduct(Long productId, boolean force) {
        ProductPO product = productDAO.findById(productId);
        if (force && product == null) {
            throw new BusinessException(ErrorCode.RECORD_NOT_FIND);
        }
        return product;
    }


    /**
     * 查询【产品管理】详情
     *
     * @param productId
     * @return
     */
    public ProductShowVO show(Long productId) {
        ProductPO product = this.getProduct(productId, true);
        ProductShowVO showVO = ProductMapper.INSTANCE.toShowVO(product);
        return showVO;
    }

    /**
     * 删除【产品管理】
     *
     * @param productIds
     * @return
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public int delete(Long... productIds) {
        int count = 0;
        for (Long productId : productIds) {
            this.checkDeleteByOrder(productId);
            this.checkDeleteByRepair(productId);
            count += productDAO.delete(productId);
        }
        return count;
    }

    /**
     * 校验是否存在【工单中心】关联
     *
     * @param productId
     */
    private void checkDeleteByOrder(Long productId) {
        int count = orderDAO.getCountByProductId(productId);
        if (count > 0) {
            throw new BusinessException(ErrorCode.CASCADE_DELETE_ERROR);
        }
    }

    /**
     * 校验是否存在【报修中心】关联
     *
     * @param productId
     */
    private void checkDeleteByRepair(Long productId) {
        int count = repairDAO.getCountByProductId(productId);
        if (count > 0) {
            throw new BusinessException(ErrorCode.CASCADE_DELETE_ERROR);
        }
    }


}


