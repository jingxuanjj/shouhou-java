package com.huangjun.shouhou.constant;

import com.huangjun.common.validator.Check;

import java.util.HashMap;
import java.util.Map;

/**
 * 枚举【工单类型】
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public enum OrderType {

    /**
     * 维修单(默认)
     */
    WEIXIU(1, "维修单(默认)"),
    /**
     * 服务单
     */
    FUWU(2, "服务单"),
    ;


    /**
     * 枚举值罗列，给swagger接口文档展示用
     */
    public static final String VALUES_STR = "1,2";

    private static final Map<Integer, OrderType> LOOKUP = new HashMap<>();

    static {
        for (OrderType e : OrderType.values()) {
            LOOKUP.put(e.value, e);
        }
    }

    private final Integer value;
    private final String desc;


    OrderType(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static OrderType find(Integer value) {
        return LOOKUP.get(value);
    }

    public static OrderType findByDesc(String desc) {
        for (OrderType e : OrderType.values()) {
            if (e.getDesc().equals(desc)) {
                return e;
            }
        }
        return null;
    }


    /**
     * desc映射value
     *
     * @param desc
     * @return
     */
    public static Integer descToValue(String desc) {
        OrderType theEnum = findByDesc(desc);
        if (theEnum != null) {
            return theEnum.getValue();
        }
        return null;
    }

    /**
     * value映射desc
     *
     * @param value
     * @return
     */
    public static String valueToDesc(Integer value) {
        OrderType theEnum = find(value);
        if (theEnum != null) {
            return theEnum.getDesc();
        }
        return null;
    }

    /**
     * 校验有效性
     */
    @Check
    public static final boolean validate(Integer value) {
        OrderType theEnum = find(value);
        return theEnum != null;
    }

    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }


}

