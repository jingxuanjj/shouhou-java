package com.huangjun.shouhou.pojo.dto;

import lombok.Data;

/**
 * 用户登录请求体
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
public class UserLoginDTO {

    private String username;

    private String password;


}

