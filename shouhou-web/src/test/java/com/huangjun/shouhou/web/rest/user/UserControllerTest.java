package com.huangjun.shouhou.web.rest.user;

import com.google.common.collect.Lists;
import com.huangjun.common.util.JsonUtil;
import com.huangjun.shouhou.help.user.UserHelper;
import com.huangjun.shouhou.pojo.dto.user.UserAddDTO;
import com.huangjun.shouhou.pojo.dto.user.UserUpdateDTO;
import com.huangjun.shouhou.pojo.po.user.UserPO;
import com.huangjun.shouhou.web.AbstractWebTest;
import com.huangjun.shouhou.web.constant.WebConst;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * 【用户表(员工表)】单元测试
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class UserControllerTest extends AbstractWebTest {

    @Autowired
    private UserHelper userHelper;


    /**
     * 新增【用户表(员工表)】
     */
    @Test
    public void save() throws Exception {
        UserAddDTO addDTO = userHelper.getUserAddDTO();
        restMockMvc.perform(post(WebConst.ModulePath.USER + "/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(addDTO)))
                .andExpect(status().isCreated());
    }

    /**
     * 修改【用户表(员工表)】
     */
    @Test
    public void update() throws Exception {
        UserPO user = userHelper.saveUserExample();
        UserUpdateDTO updateDTO = userHelper.getUserUpdateDTO(user);
        restMockMvc.perform(put(WebConst.ModulePath.USER + "/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(updateDTO)))
                .andExpect(status().isOk());
    }

    /**
     * 分页查询【用户表(员工表)】
     */
    @Test
    public void list() throws Exception {
        UserPO user = userHelper.saveUserExample();
        restMockMvc.perform(get(WebConst.ModulePath.USER + "/user"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.list.length()").value(is(1)));
    }

    /**
     * 查询【用户表(员工表)】选项列表
     */
    @Test
    public void findOptions() throws Exception {
        UserPO user = userHelper.saveUserExample();
        restMockMvc.perform(get(WebConst.ModulePath.USER + "/user/options"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(is(1)));
    }

    /**
     * 查看【用户表(员工表)】详情
     */
    @Test
    public void show() throws Exception {
        UserPO user = userHelper.saveUserExample();
        restMockMvc.perform(get(WebConst.ModulePath.USER + "/user/{userId}", user.getUserId()))
                .andExpect(status().isOk());
    }

    /**
     * 删除单个【用户表(员工表)】
     */
    @Test
    public void del() throws Exception {
        UserPO user = userHelper.saveUserExample();
        restMockMvc.perform(delete(WebConst.ModulePath.USER + "/user/{userId}", user.getUserId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }

    /**
     * 批量删除【用户表(员工表)】
     */
    @Test
    public void deleteBatch() throws Exception {
        UserPO user = userHelper.saveUserExample();
        restMockMvc.perform(delete(WebConst.ModulePath.USER + "/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(Lists.newArrayList(user.getUserId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }


}
