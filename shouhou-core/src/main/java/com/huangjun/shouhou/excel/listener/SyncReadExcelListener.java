package com.huangjun.shouhou.excel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.read.metadata.holder.ReadRowHolder;
import com.huangjun.shouhou.pojo.dto.AbstractExcelDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * 同步读取excel的监听器
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class SyncReadExcelListener<T extends AbstractExcelDTO> extends AnalysisEventListener<T> {

    private final List<T> list = new ArrayList<>();

    @Override
    public void invoke(T data, AnalysisContext context) {
        ReadRowHolder rowHolder = context.readRowHolder();
        data.setRowIndex(rowHolder.getRowIndex());
        list.add(data);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }

    public List<T> getList() {
        return list;
    }
}

