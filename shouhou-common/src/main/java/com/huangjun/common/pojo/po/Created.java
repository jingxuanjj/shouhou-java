package com.huangjun.common.pojo.po;


/**
 * 创建人&创建时间
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface Created extends CreatedBy, CreatedTime {
}

