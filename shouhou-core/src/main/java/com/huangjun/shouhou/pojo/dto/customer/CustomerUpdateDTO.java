package com.huangjun.shouhou.pojo.dto.customer;

import com.huangjun.common.pojo.dto.AbstractDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

import static com.huangjun.shouhou.pojo.example.customer.CustomerExample.*;

/**
 * 修改【客户管理】的参数
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(description = "修改【客户管理】的参数")
public class CustomerUpdateDTO extends AbstractDTO {

    @ApiModelProperty(notes = N_CUSTOMER_ID, example = E_CUSTOMER_ID, required = true)
    @NotNull
    private Long customerId;

    @ApiModelProperty(notes = N_NAME, example = E_NAME)
    @Length(max = 50)
    private String name;

    @ApiModelProperty(notes = N_NUMBER, example = E_NUMBER)
    @Length(max = 50)
    private String number;

    @ApiModelProperty(notes = N_PHONE_NAME, example = E_PHONE_NAME)
    @Length(max = 32)
    private String phoneName;

    @ApiModelProperty(notes = N_PHONE, example = E_PHONE)
    @Length(max = 32)
    private String phone;

    @ApiModelProperty(notes = N_ADDRESS, example = E_ADDRESS)
    @Length(max = 100)
    private String address;

    @ApiModelProperty(notes = N_OWNER, example = E_OWNER)
    @Length(max = 32)
    private String owner;



}

