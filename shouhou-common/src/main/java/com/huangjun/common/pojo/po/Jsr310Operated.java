package com.huangjun.common.pojo.po;


/**
 * 操作人&操作时间-jsr310时间API
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface Jsr310Operated extends OperatedBy, Jsr310OperatedTime {
}

