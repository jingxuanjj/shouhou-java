package com.huangjun.shouhou.web.api.customer;

import com.huangjun.common.pojo.qo.OptionQO;
import com.huangjun.common.pojo.vo.OptionVO;
import com.huangjun.common.pojo.vo.PageVO;
import com.huangjun.shouhou.pojo.dto.customer.CustomerAddDTO;
import com.huangjun.shouhou.pojo.dto.customer.CustomerUpdateDTO;
import com.huangjun.shouhou.pojo.qo.customer.CustomerQO;
import com.huangjun.shouhou.pojo.vo.customer.CustomerListVO;
import com.huangjun.shouhou.pojo.vo.customer.CustomerShowVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * 【客户管理】API
 * <p>swagger接口文档
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Api(tags = "【客户管理】API")
public interface CustomerAPI {

    /**
     * 新增【客户管理】
     */
    @ApiOperation(value = "新增【客户管理】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "customerAddDTO", dataTypeClass = CustomerAddDTO.class, value = "新增【客户管理】参数", paramType = "body"),
    })
    ResponseEntity<CustomerShowVO> save(CustomerAddDTO customerAddDTO) throws Exception;

    /**
     * 修改【客户管理】
     */
    @ApiOperation(value = "修改【客户管理】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "customerUpdateDTO", dataTypeClass = CustomerUpdateDTO.class, value = "修改【客户管理】参数", paramType = "body"),
    })
    ResponseEntity<CustomerShowVO> update(CustomerUpdateDTO customerUpdateDTO);

    /**
     * 分页查询【客户管理】
     */
    @ApiOperation(value = "分页查询【客户管理】")
    ResponseEntity<PageVO<CustomerListVO>> list(CustomerQO customerQO);

    /**
     * 查询【客户管理】选项列表
     */
    @ApiOperation(value = "查询【客户管理】选项列表")
    ResponseEntity<List<OptionVO<Long, String>>> findOptions(OptionQO<Long, String> qo);

    /**
     * 查看【客户管理】详情
     */
    @ApiOperation(value = "查看【客户管理】详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "customerId", dataTypeClass = Long.class, value = "【客户管理】id", paramType = "path"),
    })
    ResponseEntity<CustomerShowVO> show(Long customerId);

    /**
     * 删除单个【客户管理】
     */
    @ApiOperation(value = "删除单个【客户管理】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "customerId", dataTypeClass = Long.class, value = "【客户管理】id", paramType = "path"),
    })
    ResponseEntity<Integer> delete(Long customerId);

    /**
     * 批量删除【客户管理】
     */
    @ApiOperation(value = "批量删除【客户管理】")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", dataTypeClass = Long.class, allowMultiple = true, value = "id数组", paramType = "body"),
    })
    ResponseEntity<Integer> deleteBatch(Long[] id);

}

