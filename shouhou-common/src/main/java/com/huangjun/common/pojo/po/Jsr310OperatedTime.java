package com.huangjun.common.pojo.po;

import java.time.LocalDateTime;

/**
 * 操作时间接口-jsr310时间API
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public interface Jsr310OperatedTime {

    LocalDateTime getOperatedTime();

    void setOperatedTime(LocalDateTime operatedTime);
}

