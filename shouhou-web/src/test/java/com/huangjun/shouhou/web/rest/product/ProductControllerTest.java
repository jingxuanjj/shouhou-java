package com.huangjun.shouhou.web.rest.product;

import com.google.common.collect.Lists;
import com.huangjun.common.util.JsonUtil;
import com.huangjun.shouhou.help.product.ProductHelper;
import com.huangjun.shouhou.pojo.dto.product.ProductAddDTO;
import com.huangjun.shouhou.pojo.dto.product.ProductUpdateDTO;
import com.huangjun.shouhou.pojo.po.product.ProductPO;
import com.huangjun.shouhou.web.AbstractWebTest;
import com.huangjun.shouhou.web.constant.WebConst;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * 【产品管理】单元测试
 *
 * @author 黄俊
 * @date 2021/01/14
 */
public class ProductControllerTest extends AbstractWebTest {

    @Autowired
    private ProductHelper productHelper;


    /**
     * 新增【产品管理】
     */
    @Test
    public void save() throws Exception {
        ProductAddDTO addDTO = productHelper.getProductAddDTO();
        restMockMvc.perform(post(WebConst.ModulePath.PRODUCT + "/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(addDTO)))
                .andExpect(status().isCreated());
    }

    /**
     * 修改【产品管理】
     */
    @Test
    public void update() throws Exception {
        ProductPO product = productHelper.saveProductExample();
        ProductUpdateDTO updateDTO = productHelper.getProductUpdateDTO(product);
        restMockMvc.perform(put(WebConst.ModulePath.PRODUCT + "/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(updateDTO)))
                .andExpect(status().isOk());
    }

    /**
     * 分页查询【产品管理】
     */
    @Test
    public void list() throws Exception {
        ProductPO product = productHelper.saveProductExample();
        restMockMvc.perform(get(WebConst.ModulePath.PRODUCT + "/product"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.list.length()").value(is(1)));
    }

    /**
     * 查询【产品管理】选项列表
     */
    @Test
    public void findOptions() throws Exception {
        ProductPO product = productHelper.saveProductExample();
        restMockMvc.perform(get(WebConst.ModulePath.PRODUCT + "/product/options"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(is(1)));
    }

    /**
     * 查看【产品管理】详情
     */
    @Test
    public void show() throws Exception {
        ProductPO product = productHelper.saveProductExample();
        restMockMvc.perform(get(WebConst.ModulePath.PRODUCT + "/product/{productId}", product.getProductId()))
                .andExpect(status().isOk());
    }

    /**
     * 删除单个【产品管理】
     */
    @Test
    public void del() throws Exception {
        ProductPO product = productHelper.saveProductExample();
        restMockMvc.perform(delete(WebConst.ModulePath.PRODUCT + "/product/{productId}", product.getProductId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }

    /**
     * 批量删除【产品管理】
     */
    @Test
    public void deleteBatch() throws Exception {
        ProductPO product = productHelper.saveProductExample();
        restMockMvc.perform(delete(WebConst.ModulePath.PRODUCT + "/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJSONString(Lists.newArrayList(product.getProductId()))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }

    /**
     * 导入【产品管理】excel
     */
    @Test
    public void importExcel() throws Exception {
        // 首先下载excel模板
        MvcResult mvcResult = restMockMvc.perform(get(WebConst.ModulePath.PRODUCT + "/product/template"))
                .andExpect(status().isOk())
                .andReturn();
        MockHttpServletResponse response = mvcResult.getResponse();

        // 将模板原封不动导入
        MockMultipartFile file = new MockMultipartFile("file", response.getContentAsByteArray());
        restMockMvc.perform(multipart(WebConst.ModulePath.PRODUCT + "/product/import")
                .file(file))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(is(1)));
    }


}
