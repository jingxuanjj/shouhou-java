package com.huangjun.shouhou.pojo.dto.user;

import com.huangjun.common.pojo.dto.AbstractDTO;
import com.huangjun.common.validator.Const;
import com.huangjun.shouhou.constant.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

import static com.huangjun.shouhou.pojo.example.user.UserExample.*;

/**
 * 新增【用户表(员工表)】的参数
 *
 * @author 黄俊
 * @date 2021/01/14
 */
@Data
@EqualsAndHashCode(callSuper=true)
@ApiModel(description = "新增【用户表(员工表)】的参数")
public class UserAddDTO extends AbstractDTO {

    @ApiModelProperty(notes = N_NAME, example = E_NAME, required = true)
    @NotNull
    @Length(max = 32)
    private String name;

    @ApiModelProperty(notes = N_USERNAME, example = E_USERNAME, required = true)
    @NotNull
    @Length(max = 50)
    private String username;

    @ApiModelProperty(notes = N_PASSWORD, example = E_PASSWORD, required = true)
    @NotNull
    @Length(max = 100)
    private String password;

    @ApiModelProperty(notes = N_PHONE, example = E_PHONE)
    @Length(max = 32)
    private String phone;

    @ApiModelProperty(notes = N_ROLE, example = E_ROLE, allowableValues = Role.VALUES_STR)
    @Const(constClass = Role.class)
    private Integer role;


}


